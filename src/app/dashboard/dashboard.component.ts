import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardService } from './dashboard.services';
import { SessionStorageService } from '../common/sessionstorage';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  tableName = '';
  modalTitle: string;
  public items: any[];
  private sub: any;
  tablesub: any;
  displayedColumns = ['actions', 'code', 'Description'];
  resultsLength = 0;
  public filter: string = "";
  isLoadingResults = false;
  tableType: number = 1;
  canAdd=false;
  constructor(private route: ActivatedRoute, private router: Router, private DashboardService: DashboardService,
    vcr: ViewContainerRef,  private SessionStorageService: SessionStorageService) {
      this.canAdd = SessionStorageService.getAccess(1);


  }

  ngOnInit() {
    
  };
  ngOnDestroy() {
    
  }




}
