import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID  } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { LayoutModule } from '@angular/cdk/layout';
import { SharedModule }       from './shared/shared.module';
import { LoginService } from './login/login.services';
import { LoginComponent } from './login/login.component';
import { HTTPDataService } from './common/httpservice';
import { Http, Request, RequestOptionsArgs, Response, XHRBackend, RequestOptions, Headers } from '@angular/http';
import { SessionStorageService } from './common/sessionstorage';
import { Router } from '@angular/router';
import { AppsettingsService } from './common/appsettings';
import {ToastaModule} from 'ngx-toasta';
import { HttpModule } from '@angular/http';
import { HomeModule } from './home/home.module';
import { NgMaterialMultilevelMenuModule } from 'ng-material-multilevel-menu';

//import { CustomErrorStateMatcher } from './common/validationservice';
export function httpFactory(backend: XHRBackend, options: RequestOptions, sessionStorage: SessionStorageService, router: Router) {
  return new HTTPDataService(backend, options, sessionStorage, router);
}
const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', loadChildren: './home/home.module#HomeModule' }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ToastaModule.forRoot(),
    HttpModule,
    HomeModule,
    NgMaterialMultilevelMenuModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-AU' },
  {
    provide: HTTPDataService,
    useFactory: httpFactory,
    deps: [XHRBackend, RequestOptions, SessionStorageService, Router]
  }, SessionStorageService,  LoginService,
    AppsettingsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
