import { ValidationService } from '../common/validationservice';
import { ControlMessagesComponent } from '../common/validationservice';
import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from '../common/dialog/dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CompanyLayoutsService } from '../company-layouts/company-layouts.services';
import { DashboardService } from '../dashboard/dashboard.services';
import { MaterialModule }       from '../shared/material.module';
import { HomeService } from '../home/home.services';
import { DocumentErrorsService } from '../document-errors/document-errors.services';
import { AdminService } from '../admin/admin.services';
import { AngularDraggableModule } from 'angular2-draggable';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularDraggableModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-full-width',
      preventDuplicates: true,
    }) ],
  providers: [ValidationService,CompanyLayoutsService,HomeService,DashboardService,DocumentErrorsService, AdminService],
  entryComponents: [DialogComponent],
  declarations: [ ControlMessagesComponent,DialogComponent],
  exports:      [  ControlMessagesComponent,DialogComponent,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class SharedModule { }
