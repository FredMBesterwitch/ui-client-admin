import { NgModule, ReflectiveInjector, LOCALE_ID } from '@angular/core';
import { routing } from './home.routing';
import { CommonModule }  from '@angular/common';
import { CompanyLayoutsComponent } from '../company-layouts/company-layouts.component';
import { CompanyLayoutsEditComponent } from '../company-layouts/company-layouts-edit.component';
import { SharedModule }       from '../shared/shared.module';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { DocumentErrorsComponent } from '../document-errors/document-errors.component';
import { CanvasComponent } from '../company-layouts/canvas.component';
import { UploadFileComponent } from '../common/uploader/upload-file.component';
import { CompanyLayoutsDialog } from '../company-layouts/company-layouts-dialog.component';
import { AdminComponent } from '../admin/admin.component';
import { AdminEditComponent } from '../admin/admin-edit.component';


@NgModule({
  declarations: [
    CompanyLayoutsComponent,
    CompanyLayoutsEditComponent,
    DashboardComponent,
    DocumentErrorsComponent,
    CanvasComponent,
    UploadFileComponent,
    CompanyLayoutsDialog,
    AdminComponent,
    AdminEditComponent
  ],
  imports: [routing,CommonModule,SharedModule
  ],
  providers: [],
  entryComponents: [CompanyLayoutsDialog]

})
export class HomeModule { }
