import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HomeService } from './home.services';
import { SessionStorageService } from '../common/sessionstorage';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);
  navItems: any[] = [];
  config = {
    paddingAtStart: true,
    interfaceWithRoute: true,
    listBackgroundColor: 'white',
    fontColor: 'black',
    backgroundColor: 'white',
    highlightOnSelect: true,
    collapseOnSelect: false,
    rtlLayout: false
  };

  constructor(private router: Router, private breakpointObserver: BreakpointObserver, private HomeService: HomeService,
    private SessionStorageService: SessionStorageService) {

      var menuItems = [];
      menuItems.push({ 'label': "DashBoard", 'link': '/home/dashboard' });
      menuItems.push({ 'label': "Document Errors", 'link': '/home/document-errors' });
      menuItems.push({ 'label': "Company Layouts", 'link': '/home/company-layouts' });
      menuItems.push({ 'label': "Admin", 'items': [{ 'label': 'Document Type', 'link': '/home/admin/lookuptables/1'},
                                                   { 'label': 'Transport Method', 'link': '/home/admin/lookuptables/2'},
                                                   { 'label': 'Security Type', 'link': '/home/admin/lookuptables/3'},
                                                   { 'label': 'Data Type', 'link': '/home/admin/lookuptables/4'}] });

      //this.navItems = SessionStorageService.getItem("Menu");
      this.navItems =  menuItems;
    }

  btnClick() {
    console.log("Navigtae");
  }
  logout() {
    this.HomeService.logout()
      .subscribe(
        userdata => {
          this.SessionStorageService.setItem("auth_token", "")
          this.SessionStorageService.setItem("UserName", "");
          this.router.navigate(['login']);
        },
        err => {
          console.log(err);
        });
  }
}
