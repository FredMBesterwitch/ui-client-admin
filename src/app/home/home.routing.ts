import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HomeComponent } from '../home/home.component';
import { CompanyLayoutsComponent } from '../company-layouts/company-layouts.component';
import { CompanyLayoutsEditComponent } from '../company-layouts/company-layouts-edit.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { DocumentErrorsComponent } from '../document-errors/document-errors.component';
import { AdminComponent } from '../admin/admin.component';
import { AdminEditComponent } from '../admin/admin-edit.component';
export const routes: Routes = [
  {
    path: 'home', component: HomeComponent,
    children: [
      { path: '', redirectTo: 'company-layouts', pathMatch: 'full' },
      { path: 'company-layouts', component: CompanyLayoutsComponent },
      { path: 'company-layouts/:id/edit', component: CompanyLayoutsEditComponent },
      { path: 'document-errors', component: DocumentErrorsComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'admin', component: AdminComponent },
      { path: 'admin/lookuptables/:lookuptableType', component: AdminComponent },
      { path: 'admin/lookuptables/:id/edit', component: AdminEditComponent },

    ],
    runGuardsAndResolvers: 'always',
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
