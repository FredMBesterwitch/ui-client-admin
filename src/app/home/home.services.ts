import { Injectable } from '@angular/core';
import { HTTPDataService } from '../common/httpservice';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

@Injectable()
export class HomeService {
    constructor(private httpservice: HTTPDataService) {

    }

    logout(): Observable<any> {
        return this.httpservice.logout();
    }
}