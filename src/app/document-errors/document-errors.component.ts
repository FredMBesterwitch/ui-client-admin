import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DocumentErrorsService } from './document-errors.services';
import { SessionStorageService } from '../common/sessionstorage';

@Component({
  selector: 'document-errors',
  templateUrl: './document-errors.component.html',
  styleUrls: ['./document-errors.component.css']
})
export class DocumentErrorsComponent implements OnInit {
  tableName = '';
  modalTitle: string;
  public items: any[];
  private sub: any;
  tablesub: any;
  displayedColumns = ['actions', 'code', 'Description'];
  resultsLength = 0;
  public filter: string = "";
  isLoadingResults = false;
  tableType: number = 1;
  canAdd=false;
  constructor(private route: ActivatedRoute, private router: Router, private DocumentErrorsService: DocumentErrorsService,
    vcr: ViewContainerRef,  private SessionStorageService: SessionStorageService) {
      this.canAdd = SessionStorageService.getAccess(1);


  }

  ngOnInit() {
    
  };
  ngOnDestroy() {
    
  }




}
