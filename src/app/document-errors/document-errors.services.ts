import { Injectable } from '@angular/core';
import { HTTPDataService } from '../common/httpservice';
import { Observable, of } from 'rxjs';
import { map } from "rxjs/operators";
import { SessionStorageService } from '../common/sessionstorage';
import * as _ from 'lodash';

@Injectable()
export class DocumentErrorsService {
    public items: any[] = [];
    data: any = {};
    constructor(private httpservice: HTTPDataService, private SessionStorageService: SessionStorageService) {

    }
    getTables(page: number, pagesize: number, sortby: string, sortorder: boolean, searchText: string, tableType: number): Observable<any> {
        this.data = {};
        var lookupTables = this.SessionStorageService.getItem("LookUpTables");
        var _sortorder = sortorder ? "desc" : "asc";
        console.log("sortby", sortby);
        console.log("_sortorder", _sortorder);
        searchText = searchText.toLowerCase();
        this.data.data = _.filter(lookupTables, function (dataType) {
            if (searchText == "") return dataType.dataType == tableType

            return dataType.dataType == tableType
                && (dataType.code.toLowerCase().indexOf(searchText) >= 0
                    || dataType.Description.toLowerCase().indexOf(searchText) >= 0);
        });
        this.data.recordCount = this.data.data.length;
        this.data.data = _.orderBy(this.data.data, [sortby], [_sortorder]);
        this.data.data = _.drop(this.data.data, (page - 1) * pagesize).slice(0, pagesize);

        return of(this.data);

    }

    getTable(id: string, tableType: number): Observable<any> {
        this.data = {};
        console.log("id:", id);
        if (id === "0") {
            this.data.code = "";
            this.data.Description = "";
            this.data.childAnalysis = "";
        }
        else {
            var lookupTables = this.SessionStorageService.getItem("LookUpTables");
            this.data = _.find(lookupTables, function (dataType) {
                return dataType.dataType == tableType
                    && dataType.code == id;
            });
        }
        this.data.dataType = tableType;
        return of(this.data);
    }
    Save(ViewModel: any, tableType: number): Observable<any> {
        var Url = 'LookupTable/saveitem';
        console.log(ViewModel);
        return this.httpservice.post(Url, JSON.stringify(ViewModel))
            .pipe(map((res: any) => {
                console.log("res", res);
                if (res.json() === true) {
                    this.data = {};
                    var lookupTables = this.SessionStorageService.getItem("LookUpTables");
                    this.data = _.find(lookupTables, function (dataType) {
                        return dataType.dataType == tableType &&
                            dataType.code == ViewModel.LookupTable.code;
                    });
                    console.log("this.data",this.data);
                    if (this.data === null || this.data === undefined) {
                        console.log("Data Added back to the Array");
                        lookupTables.push({ "code": ViewModel.LookupTable.code, "Description": ViewModel.LookupTable.Description, "childAnalysis": ViewModel.LookupTable.childAnalysis, "dataType": tableType });
                    }
                    else {
                        console.log("Data updated back to the Array");
                        this.data.code = ViewModel.LookupTable.code;
                        this.data.Description = ViewModel.LookupTable.Description;
                        this.data.childAnalysis = ViewModel.LookupTable.childAnalysis;
                    }
                    this.SessionStorageService.setItem("LookUpTables", lookupTables);
                }
                return res.json();
            }));
    }

    ItemCodeExists(itemcode: string, tableType: number): Observable<any> {
       
        itemcode=itemcode.toLowerCase();
        var lookupTables = this.SessionStorageService.getItem("LookUpTables");
        console.log("lookupTables",lookupTables);
        console.log("tableType",tableType);
        console.log("itemcode",itemcode.length);
        var returnval=true;
        let data = _.findIndex(lookupTables, function (o) {
            return o.dataType == tableType &&
                o.code.toLowerCase() == itemcode;
        });
        console.log("this.data",data);
        if (data === null || data === undefined || data===-1) {
            returnval= false;
        }
        return of(returnval);
    }


}