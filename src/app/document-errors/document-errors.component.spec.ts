import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentErrorsComponent } from './document-errors.component';

describe('GroupAccessComponent', () => {
  let component: DocumentErrorsComponent;
  let fixture: ComponentFixture<DocumentErrorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentErrorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentErrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
