import { Injectable } from '@angular/core';
import { HTTPDataService } from '../common/httpservice';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

@Injectable()
export class LoginService {
    constructor(private httpservice: HTTPDataService) {

    }

    login(email: string, password: string): Observable<any> {
        return this.httpservice.login(email, password)
        .pipe(map((res: any) => res.json()));
    }
    getUserDetails(): Observable<any> {
        return this.httpservice.get("home/getUserDetails")
        .pipe(map((res: any) => res.json()));
    }
    logout(): Observable<any> {
        return this.httpservice.logout()
            ;
    }
}