import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ViewContainerRef, Input, OnChanges } from '@angular/core';
import { LoginService } from './login.services';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionStorageService } from '../common/sessionstorage';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../common/validationservice';
import { ToastrService } from 'ngx-toastr';

declare var resetPWDURL: any;
declare var appConfig: any;

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']

})
export class LoginComponent implements OnChanges {

    loginForm: any;
    public loading: boolean = false;
    public AppName: string = "";
    public AppLogo: string = "";
    isLoadingResults=false;
    constructor(private fb: FormBuilder, private LoginService: LoginService, private route: ActivatedRoute,
        private router: Router, private toastr: ToastrService,
        private SessionStorageService: SessionStorageService) {

        this.AppName = appConfig.AppName;
        this.AppLogo = appConfig.AppLogo;

        this.createForm();

    }
    createForm() {
        this.loginForm = this.fb.group({
            logon: ['', [Validators.required]],
            password: ['', Validators.required],
        });
    }
    ngOnChanges() {
    }

    onSubmit() {
        console.log("In Login");
        this.loading = true;
        if (this.loginForm.controls.logon.value === null || this.loginForm.controls.logon.value === "" ||
            this.loginForm.controls.password.value === null || this.loginForm.controls.password.value === "") {
            return;
        }
        this.isLoadingResults=true;
        this.LoginService.login(this.loginForm.controls.logon.value, this.loginForm.controls.password.value)
            .subscribe(
                data => {
                    console.log("token:" + data.access_token);
                    this.SessionStorageService.setItem("auth_token", data.access_token);
                    this.LoginService.getUserDetails()
                        .subscribe(
                            userdata => {
                                this.SessionStorageService.setItem("LookUpTables", userdata.ForeignTables);
                                this.SessionStorageService.setItem("DataSources", userdata.DataSources);
                                this.SessionStorageService.setItem("Menu", userdata.Menu);
                                this.SessionStorageService.setItem("Access", userdata.Access);
                                this.router.navigate(['home']);
                                this.isLoadingResults=false;
                            },
                            err => {
                                this.isLoadingResults=false;
                                this.toastr.error("You Don't have access to this application!!!", 'Login Error');
                                console.log(err);
                            });
                },
                err => {
                    this.toastr.error('Your Logon or Password is not right!!!', 'Login Error');
                    this.isLoadingResults=false;
                    console.log("401 error:", err);
                });


    }

}
