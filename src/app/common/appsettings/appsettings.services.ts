import { Injectable } from '@angular/core';
import { SessionStorageService } from '../../common/sessionstorage';

declare var sessionStorage: any;

@Injectable()
export class AppsettingsService {
    constructor(private SessionStorageService:SessionStorageService)
    {

    }
    getAppSetting(appItem: string): any {
        try {
            var sessionStorage = this.SessionStorageService.getItem("CPL_UserDetails");
            return sessionStorage[appItem];
        } catch (error) {
        }
        return "";
    };

    setItem(storageItem: string, itemValue: any) {
        try {
            sessionStorage.setItem(storageItem, JSON.stringify(itemValue));
        } catch (error) {
        }
    };
}