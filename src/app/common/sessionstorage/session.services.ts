import { Injectable } from '@angular/core';
declare var sessionStorage: any;

@Injectable()
export class SessionStorageService {
    getItem(storageItem: string): any {
        try {
            return JSON.parse(sessionStorage.getItem(storageItem));
        } catch (error) {
        }
        return "";
    };

    setItem(storageItem: string, itemValue: any) {
        try {
            sessionStorage.setItem(storageItem, JSON.stringify(itemValue));
        } catch (error) {
        }
    };

    getAccess(accessType:number):boolean{
        return true;
        var access = this.getItem("Access");
        return access.substr(accessType,1) == "1";
    }
}