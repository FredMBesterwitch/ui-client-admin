import {
  Component, Input, Output, ElementRef, AfterViewInit, ViewChild, EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent {
  @Input() public allowedFileTypes = "";
  @Input() public maxFiles = 1;
  @Input() public maxFileSize = 5000000;
  @Input() public fileUploaderCaption = "Drag File here or click to Open File Chooser";
  @Output() fileUploaderChanged = new EventEmitter<any>();

  files: any = [];
  errorMsg = '';

  public getFiles(): any[] {
    return this.files;
  }
  uploadFile(event) {
    var currentFileCount=this.files.length;
    var aAllowedFiletypes=[];
    this.errorMsg = '';
    if(this.allowedFileTypes !== null && this.allowedFileTypes !==""  && this.allowedFileTypes !==undefined)
    {
      aAllowedFiletypes = this.allowedFileTypes.split(";");
    }
    console.log("aAllowedFiletypes:", aAllowedFiletypes);
    for (let index = 0; index < event.length; index++) {
      currentFileCount++;
      const element = event[index];
      // check if Files count   <= maxFiles
      if (currentFileCount <= this.maxFiles) {

        // check mime Type
        var extension = element.name.split('.').pop();
        console.log("extension:", extension);
        if (aAllowedFiletypes.length <= 0 || aAllowedFiletypes.includes(extension)) {
          if (element.size <= this.maxFileSize) {
            console.log("event:", event);
            var that = this;
            var reader = new FileReader();
            reader.onloadend = function () {
              console.log("File Content:", reader.result);
              that.files.push({ 'fileName': element.name, 'data': reader.result, 'fileType': extension});
              console.log("that.files:",that.files);
              that.fileUploaderChanged.emit(that.files);
            };
            if (extension == 'csv') {
              reader.readAsText(element, "UTF-8")
            } else {
              reader.readAsDataURL(element);
            }
          }
          else {
            this.errorMsg = "File Too Big...";
            console.log("File Too Big...")
          }
        }
        else {
          this.errorMsg = "Only Allowed " + this.allowedFileTypes;
          console.log("Extension Not Allowed...");
        }
      }
      else {
        this.errorMsg = "Exceeded File Limit...";
        console.log("Exceeded File Limit...");
      }
    }
  }
  deleteAttachment(index) {
    this.files.splice(index, 1);
    this.fileUploaderChanged.emit(this.files);
  }

}
