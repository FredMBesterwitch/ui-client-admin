import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Http, Request, RequestOptionsArgs, Response, XHRBackend, RequestOptions, Headers, ResponseContentType } from '@angular/http';
import { Router } from '@angular/router';
// Import RxJs required methods
import { SessionStorageService } from '../sessionstorage';
declare var appConfig: any;
declare var btoa: any;

@Injectable()
export class HTTPDataService extends Http {
    private rootURL: string = appConfig.rootWebApiUrl;
    private clientid: string = appConfig.ClientID;
    private formEncoding: string = "application/x-www-form-urlencoded";

    constructor(backend: XHRBackend, options: RequestOptions, private sessionStorage: SessionStorageService, private router: Router) {
        super(backend, options);

    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        let apiURL: string | Request = '';
        let token = this.sessionStorage.getItem('auth_token');
        
        if (typeof url === 'string') {
            apiURL = this.rootURL.concat(url);
            if (!options) {
                // let's make option object
                options = { headers: new Headers() };
            }
            console.log("Options:",url);
            this.setHeaderOptions(options, token);
        }
        else {
            if (url.url.search("token") == -1 && url.url.search("logout") == -1) {
                url.url = this.rootURL.concat(url.url);
                this.setHeaderOptions(url, token);
            }
            apiURL = url;
        }
        console.log(apiURL);
        return super.request(apiURL, options).pipe(catchError(this.catchAuthError(this)));
    }

    setHeaderOptions(options: any, token:string ) {
        options.headers.set('Authorization', `Bearer ${token}`);
        options.headers.set('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0');
        options.headers.set('Cache-Control', 'post-check=0, pre-check=0, false');
        options.headers.set('Pragma', 'no-cache');
    }
    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.get(url, options);
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        // url = this.rootURL + url;
        console.log(url);
        return super.post(url, body, this.getRequestOptionArgs(options)).pipe(catchError(this.catchAuthError(this)));
    }

    login(email: string, password: string): Observable<Response> {
        //url: string, body: string, options?: RequestOptionsArgs
        let authURL: string = appConfig.authorizeURL;
        this.clientid = email+":"+password;
        let body: string ="";// "username=" + encodeURIComponent(email) + "&password=" + encodeURIComponent(password) +
            //"&grant_type=" + encodeURIComponent("password") + "&scope=aud+openid+profile+email+read+write";
        let options: RequestOptionsArgs = new RequestOptions();
        this.setAuthoriseHeaders(options);
        return super.post(authURL, body, this.getRequestOptionArgs(options, true)).pipe(catchError(this.catchAuthError(this)));
    }
    logout(): Observable<Response> {
        //url: string, body: string, options?: RequestOptionsArgs
        let authURL: string = appConfig.revokeURL;
        let body: string = "";
        let options  = { headers: new Headers() };
        let token = this.sessionStorage.getItem('auth_token');
        this.setHeaderOptions(options, token);
        return super.post(authURL, body, this.getRequestOptionArgs(options, true)).pipe(catchError(this.catchAuthError(this)));
    }
    print(url: string, options?: RequestOptionsArgs): Observable<Response> {
        //url = this.rootURL + url;
        //options.headers = new Headers();
        //options.headers.append('Authorization', "Basic " + btoa(this.clientid));
        //options.headers.append('Content-Type', "application/pdf");
        return super.get(url,{responseType: ResponseContentType.Blob });
    }
    setAuthoriseHeaders(options: RequestOptionsArgs) {
        console.log("Client ID:",this.clientid);
        options.headers = new Headers();
        options.headers.append('Authorization', "Basic " + btoa(this.clientid));
        options.headers.append('Content-Type', this.formEncoding);
    }
    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.put(this.rootURL + url, body, this.getRequestOptionArgs(options)).pipe(catchError(this.catchAuthError(this)));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.delete(this.rootURL + url, options).pipe(catchError(this.catchAuthError(this)));
    }

    getRequestOptionArgs(options?: RequestOptionsArgs, login?: boolean): RequestOptionsArgs {

        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        if (!login) {
            options.headers.append('Content-Type', 'application/json');
        }
        return options;
    }
    private catchAuthError(self: HTTPDataService) {
        return (res: Response) => {
            if (res.status === 401 || res.status === 403) {
                this.router.navigate(['login']);
            }
            return Observable.throw(res);
        };
    }
}