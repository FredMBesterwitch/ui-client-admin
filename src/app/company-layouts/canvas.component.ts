import {
  Component, Input, Output, ElementRef, AfterViewInit, ViewChild, EventEmitter
} from '@angular/core';
import { fromEvent } from 'rxjs';
import { switchMap, takeUntil, pairwise } from 'rxjs/operators'
declare var $: any;
@Component({
  selector: 'app-canvas',
  template: '<div style="position:relative;" #pdf_canvas></div>',
  styles: ['canvas { border: 1px solid #000; }']
})
​
​
export class CanvasComponent implements AfterViewInit {
​
  firstClick = false;
  canvasEl: HTMLDivElement;
  drawStartX = 0;
  drawStartY = 0;
  drawingRect: HTMLDivElement;
  lastid = "";
  divStartMoving = false;
  onDiv = false;
  offset = [0, 0];
  resizeStartMoving = false;
  resizeonDiv = false;
  backgroundImage: any;
  currentImageDimensions: any = {};
  @ViewChild('pdf_canvas') public canvas: ElementRef;
​
  @Input() public lockCanvas:boolean = false;
  @Input() public source = "";
  @Output() afterCreateRectangle = new EventEmitter<any>();
  @Output() afterMove = new EventEmitter<any>();
  @Output() afterResize = new EventEmitter<any>();
  @Output() onSelect = new EventEmitter<any>();
  @Output() deleteRectangle = new EventEmitter<any>();
  @Output() canvasReady = new EventEmitter<any>();
  private cx: CanvasRenderingContext2D;
​
  public ngAfterViewInit() {
    this.canvasEl = this.canvas.nativeElement;
    this.canvasEl.style.maxWidth = "800px";
    this.canvasEl.style.height = "auto";
    this.canvasEl.addEventListener("mousedown", this.mouseDown);
    // this.canvasEl.addEventListener("mousemove", this.mouseMove);
    // this.canvasEl.addEventListener("mouseup", this.mouseUp);
    this.backgroundImage = new Image();
    //"/assets/images/document_800_1.png";
    var that = this;
    if (this.source) {
      this.backgroundImage.src = this.source;
      this.canvasEl.style.backgroundImage = "url('" + that.source + "')";
      this.backgroundImage.onload = function () {
        console.log("Image Size:", that.backgroundImage.width, that.backgroundImage.height);
        var aspectRatio = that.calculateAspectRatioFit(that.backgroundImage.width, that.backgroundImage.height);
        console.log("aspectRatio:", aspectRatio);
​
        //that.backgroundImage.height = aspectRatio.height;
        that.canvasEl.style.width = aspectRatio.width.toString() + "px";
        that.canvasEl.style.height = (aspectRatio.height).toString() + "px";
        that.canvasEl.style.backgroundSize = "100% 100%";
        console.log("canvasEl:", that.canvasEl);
        that.canvasReady.emit({ canvas: true});
      }
    }
  }
  calculateAspectRatioFit(srcWidth, srcHeight) {
    var ratio = Math.min(800 / srcWidth, 3500 / srcHeight);
    this.currentImageDimensions.width = srcWidth * ratio;
    this.currentImageDimensions.height = srcHeight * ratio;
    return { width: srcWidth * ratio, height: srcHeight * ratio };
  }
  public setBackgroundImage() {
    if (this.source) {
      this.backgroundImage.src = this.source;
      this.canvasEl.style.backgroundImage = "url('" + this.source + "')";
      this.onDiv = false;
    }
  }
  public addRectangle(rectangle: any) {
    this.lastid = rectangle.id;
    this.createRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h);
    this.addContentToDiv();
    console.log("In Add Rectangle..", rectangle);
  }
  public addRectangles(rectangles: any) {
    var that = this;
    rectangles.forEach(function (rectangle) {
      console.log("rectangle", rectangle);
      that.lastid = rectangle.id;
      that.createRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h);
      that.addContentToDiv();
    });
​
​
  }
  mouseDown = (ev: MouseEvent) => {
    if(this.lockCanvas) return ;
    console.log("mouseDown:this.onDiv", this.onDiv);
    if (this.onDiv) return;
    this.startDrawRect(ev);
​
  }
​
  startDrawRect(e) {
    const rect = this.canvasEl.getBoundingClientRect();
    // Save start positions
    this.drawStartX = e.x - rect.left;
    this.drawStartY = e.y - rect.top;
    this.lastid = this.revisedRandId();
    this.createRect(this.drawStartX, this.drawStartY, 0, 0);
    this.canvasEl.addEventListener("mousemove", this.drawRect);
    this.canvasEl.addEventListener("mouseup", this.endDrawRect);
  }
  drawRect = (ev: MouseEvent) => {
    const rect = this.canvasEl.getBoundingClientRect();
    var drawEndX = ev.x - rect.left;
    var drawEndY = ev.y - rect.top;
    var position = this.calculateRectPos(this.drawStartX, this.drawStartY, drawEndX, drawEndY);
    console.log("Position:",position);
    this.drawingRect.style.top = position.top + "px";
    this.drawingRect.style.left = position.left + "px";
    this.drawingRect.style.width = position.width + "px";
    this.drawingRect.style.height = position.height + "px";
    console.log("drawingRect.style:",this.drawingRect.style);
  }
  calculateRectPos = (startX, startY, endX, endY) => {
    var width = endX - startX;
    var height = endY - startY;
    var posX = startX;
    var posY = startY;

    if (width < 0) {
      width = Math.abs(width);
      posX -= width;
    }
​
    if (height < 0) {
      height = Math.abs(height);
      posY -= height;
    }

    return {
      left: posX,
      top: posY,
      width: width,
      height: height
    };
​
  }
  public revisedRandId() {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
  }
  public clearAllSelections() {
    var divsFound = $(this.canvasEl).children().remove(".field-marker");
​
    //debugger;
  }
  public getAllSelections() {
    var divs = [];
    var that = this;
    $(this.canvasEl).find(".field-marker").each(function () {

      var newRectangle: any = {};
      //var div = (<HTMLDivElement><any>that.canvasEl.getElementsByClassName(that.lastid));
      var drawingRect: HTMLDivElement = this; // div[0];
      newRectangle.x = Math.floor((that.backgroundImage.width / that.currentImageDimensions.width) * parseInt(drawingRect.style.left));
      newRectangle.y = Math.floor((that.backgroundImage.height / that.currentImageDimensions.height) * parseInt(drawingRect.style.top));
      newRectangle.r = newRectangle.x + Math.ceil((that.backgroundImage.width / that.currentImageDimensions.width) * parseInt(drawingRect.style.width));
      newRectangle.b = newRectangle.y + Math.ceil((that.backgroundImage.height / that.currentImageDimensions.height) * parseInt(drawingRect.style.height));
      newRectangle.id = drawingRect.id;
      newRectangle.top = parseInt(drawingRect.style.top);
      newRectangle.left = parseInt(drawingRect.style.left);
      newRectangle.width = parseInt(drawingRect.style.width);
      newRectangle.height = parseInt(drawingRect.style.height);
​
      //divs.push({ x: $(this).position().left, y: $(this).position().top, w: $(this).width(), h: $(this).height(), id: $(this).attr('id') });
      divs.push(newRectangle);
​
    });
    return divs;
  }
  createRect = (x, y, w, h) => {
    var userHtml = "<div id='" + this.lastid + "' class='field-marker " + this.lastid + "' style='left:" + x + "px;top:" + y + "px;width:" + w + "px;height:" + h + "px;'></div>";
    this.canvasEl.insertAdjacentHTML('beforeend', userHtml);
    var div = (<HTMLDivElement><any>this.canvasEl.getElementsByClassName(this.lastid));//document.getElementById('content');
    this.drawingRect = div[0];
​
​
    console.log("this.drawingRect", this.drawingRect);
  }
  endDrawRect = (ev: MouseEvent) => {
    const rect = this.canvasEl.getBoundingClientRect();
    var drawEndX = ev.x - rect.left;
    var drawEndY = ev.y - rect.top;
    var position = this.calculateRectPos(this.drawStartX, this.drawStartY, drawEndX, drawEndY);
    var that = this;
    if (position.width < 10 || position.height < 5) {
      this.drawingRect.remove();
    }
    else {
      this.addContentToDiv();
    }
    console.log("Start:", this.drawStartX, this.drawStartY);
    console.log("End:", drawEndX, drawEndY);
    this.canvasEl.removeEventListener("mousemove", this.drawRect);
    this.canvasEl.removeEventListener("mouseup", this.endDrawRect);
  }
  addContentToDiv() {
    var that = this;
    var userHtml = '';// '<div class="resize-handle n-border"></div><div class="resize-handle s-border"></div><div class="resize-handle w-border"></div><div class="resize-handle e-border"></div><div class="resize-handle nw-border"></div><div class="resize-handle sw-border"></div><div class="resize-handle se-border"></div><div class="resize-handle ne-border"></div>';
    userHtml += "<button class='field-marker-button-" + this.lastid + "' name='close' >×</button>";
    var div = (<HTMLDivElement><any>this.canvasEl.getElementsByClassName(this.lastid));
    var drawingRect: HTMLDivElement = div[0];
    drawingRect.insertAdjacentHTML('beforeend', userHtml);
    // drawingRect.style.border = "3px solid rgba(199, 25, 9, 0.64)"
    // convert to actuals
    console.log("drawingRect:", drawingRect);
​
    var newRectangle: any = {};
​
    newRectangle.x = Math.floor((this.backgroundImage.width / this.currentImageDimensions.width) * parseInt(this.drawingRect.style.left));
    newRectangle.y = Math.floor((this.backgroundImage.height / this.currentImageDimensions.height) * parseInt(this.drawingRect.style.top));
    newRectangle.r = newRectangle.x + Math.ceil((this.backgroundImage.width / this.currentImageDimensions.width) * parseInt(this.drawingRect.style.width));
    newRectangle.b = newRectangle.y + Math.ceil((this.backgroundImage.height / this.currentImageDimensions.height) * parseInt(this.drawingRect.style.height));
    newRectangle.id = that.drawingRect.id;
    newRectangle.top = parseInt(this.drawingRect.style.top);
    newRectangle.left = parseInt(this.drawingRect.style.left);
    newRectangle.width = parseInt(this.drawingRect.style.width);
    newRectangle.height = parseInt(this.drawingRect.style.height);
​    this.changeDivCss(drawingRect, '');
    this.afterCreateRectangle.emit({ rectangle: newRectangle });
    this.onDiv = false;
    $(drawingRect).resizable({
      handles: "all",
      start: function (event, ui) {
        this.enforceMinWidth = $(this).resizable("option", "minWidth");
      },
      resize: function (event, ui) {
        console.log("inside resize")
        if (that.lockCanvas) {
          console.log("lock screen here")
          $(this).resizable("option", "maxWidth", ui.size.width);
        }
      },
      stop: function (event, ui) {
        var newRectangle: any = {};
        var div = (<HTMLDivElement><any>that.canvasEl.getElementsByClassName(that.lastid));
        var drawingRect: HTMLDivElement = div[0];
        newRectangle.x = Math.floor((that.backgroundImage.width / that.currentImageDimensions.width) * parseInt(that.drawingRect.style.left));
        newRectangle.y = Math.floor((that.backgroundImage.height / that.currentImageDimensions.height) * parseInt(that.drawingRect.style.top));
        newRectangle.r = newRectangle.x + Math.ceil((that.backgroundImage.width / that.currentImageDimensions.width) * parseInt(that.drawingRect.style.width));
        newRectangle.b = newRectangle.y + Math.ceil((that.backgroundImage.height / that.currentImageDimensions.height) * parseInt(that.drawingRect.style.height));
        newRectangle.id = that.drawingRect.id;
        newRectangle.top = parseInt(that.drawingRect.style.top);
        newRectangle.left = parseInt(that.drawingRect.style.left);
        newRectangle.width = parseInt(that.drawingRect.style.width);
        newRectangle.height = parseInt(that.drawingRect.style.height);
​        that.changeDivCss(drawingRect, newRectangle.id);
        that.afterResize.emit({ rectangle: newRectangle });
      },
      containment: $(that.canvasEl)
    });
    $(drawingRect).draggable({
      revert: function (event, ui) {
        if (that.lockCanvas) {
          return true;
        }
      },
      stop: function (event, ui) {
        if (that.lockCanvas) {
          console.log("lock screen", event, ui);
          $(drawingRect).draggable("option", "revert", true)
        }
        var newRectangle: any = {};
        var div = (<HTMLDivElement><any>that.canvasEl.getElementsByClassName(that.lastid));
        var drawingRect: HTMLDivElement = div[0];
        newRectangle.x = Math.floor((that.backgroundImage.width / that.currentImageDimensions.width) * parseInt(that.drawingRect.style.left));
        newRectangle.y = Math.floor((that.backgroundImage.height / that.currentImageDimensions.height) * parseInt(that.drawingRect.style.top));
        newRectangle.r = newRectangle.x + Math.ceil((that.backgroundImage.width / that.currentImageDimensions.width) * parseInt(that.drawingRect.style.width));
        newRectangle.b = newRectangle.y + Math.ceil((that.backgroundImage.height / that.currentImageDimensions.height) * parseInt(that.drawingRect.style.height));
        newRectangle.id = that.drawingRect.id;
        newRectangle.top = parseInt(that.drawingRect.style.top);
        newRectangle.left = parseInt(that.drawingRect.style.left);
        newRectangle.width = parseInt(that.drawingRect.style.width);
        newRectangle.height = parseInt(that.drawingRect.style.height);
        that.changeDivCss(drawingRect, newRectangle.id);
        that.afterMove.emit({ rectangle: newRectangle });
      },
      containment: $(that.canvasEl)
    });
​
    drawingRect.addEventListener('mouseenter', function (e) {
      that.onDiv = true;
    }, true);
    drawingRect.addEventListener('mouseover', function (e) {
      that.onDiv = true;
    }, true);
    drawingRect.addEventListener('mouseleave', function (e) {
      that.onDiv = false;
    }, true);
    drawingRect.addEventListener('click', function (e) {
      that.onDiv = false;
      that.changeDivCss(drawingRect, newRectangle.id);
      that.onSelect.emit({ rectangle: newRectangle });
    }, true);
    var btndiv = (<HTMLDivElement><any>this.canvasEl.getElementsByClassName('field-marker-button-' + this.lastid));
    var btn = btndiv[0];
    btn.addEventListener("click", (e: Event) => this.deleteDiv(e, newRectangle));
  }
  deleteDiv = (e, rectangle) => {
    if(this.lockCanvas) return ;
    console.log("Deleted DIV:", e);
    e.srcElement.parentElement.remove();
    this.onDiv = false;
    this.deleteRectangle.emit({ rectangle: rectangle });
​
  }
  showdeleteButton(e) {
    console.log("Deleted DIV:", e);
  }
  changeDivCss(drawingRect, id) {
    if(this.lockCanvas) return ;
    var that = this;
    $(this.canvasEl).find(".field-marker").each(function () {
      let currentDiv: HTMLDivElement = this;
      currentDiv.style.border = "1px solid rgba(199, 25, 9, 0.64)";
    });
    drawingRect.style.border = "3px solid rgba(199, 25, 9, 0.64)";

    let btns = (document.getElementsByName('close'));
    console.log(btns)
    for (let i=0; i< btns.length; i++) {
      btns[i].style.display = 'none';
    }

    if (id != '') {
      let btndiv = (<HTMLDivElement><any>this.canvasEl.getElementsByClassName('field-marker-button-' + id));
      let btn = btndiv[0];
      console.log(btn)
      btn.style.display = 'inline-block';
    }

  }
}
