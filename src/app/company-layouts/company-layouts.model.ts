export const ConstPageArray = [
  { pageNo:1, fieldArray:[] },
  { pageNo:2, fieldArray:[] },
  { pageNo:3, fieldArray:[] },
  { pageNo:4, fieldArray:[] },
  { pageNo:5, fieldArray:[] },
  { pageNo:6, fieldArray:[] },
  { pageNo:7, fieldArray:[] }
]

export const ConstColumn = [
  {id: 1, value: "Column 1"},
  {id: 2, value: "Column 2"},
  {id: 3, value: "Column 3"},
  {id: 4, value: "Column 4"},
  {id: 5, value: "Column 5"},
  {id: 6, value: "Column 6"},
  {id: 7, value: "Column 7"},
  {id: 8, value: "Column 8"},
  {id: 9, value: "Column 9"},
  {id: 10, value: "Column 10"},
  {id: 11, value: "Column 11"},
  {id: 12, value: "Column 12"},
  {id: 13, value: "Column 13"},
  {id: 14, value: "Column 14"},
  {id: 15, value: "Column 15"},
  {id: 16, value: "Column 16"},
  {id: 17, value: "Column 17"},
  {id: 18, value: "Column 18"},
  {id: 19, value: "Column 19"},
  {id: 20, value: "Column 20"}
]

export const ConstCustomerModel = {
  ItemCode: '',
  CompanyName: '',
  Documents: [],
}

export const ConstFieldFormModel = {
  ID: '',
  FieldName: '',
  ShowLayout: true,
  Caption: '',
  DataType: '',
  DateValue: 'DD/MM/YY',
  MinValue: 0,
  MaxValue: 999999,
  DefaultValue: '',
  EmptyValue: false,
  Output_DataType: '',
  Output_DateValue: 'DD/MM/YY',
  Output_MinValue: 0,
  Output_MaxValue: 999999,
  Output_DefaultValue: '',
  Output_EmptyValue: false,
  x: 0,
  y: 0,
  b: 0,
  r: 0,
  top_int: 0,
  left_int: 0,
  width: 0,
  height: 0,
  tabularFieldArray: []
}

export const ConstColumnFormModel = {
  ColumnID: 0,
  FieldName: '',
  ShowLayout: true,
  Caption: '',
  DataType: '',
  DateValue: 'DD/MM/YY',
  MinValue: 0,
  MaxValue: 999999,
  DefaultValue: '',
  EmptyValue: false,
  Output_DataType: '',
  Output_DateValue: 'DD/MM/YY',
  Output_MinValue: 0,
  Output_MaxValue: 999999,
  Output_DefaultValue: '',
  Output_EmptyValue: false,
  TablePosition: 'Table Fixed',
  TableTopValue: 'Left Most Text Before table Starts',
  TableBottomValue: 'Left Most Text After Table Ends'
}

export const ConstDocumentModel = {
  ShowDocument: true,
  DocumentName : 'Document Name',
  DocumentDescription : 'Document Description',
  InputDocumentType : "1",
  InputMethod : "10",
  OutputDocumentType : "1",
  OutputMethod : "10",
  InputSecurityType : "11",
  IncomingEmail : 'email@gmail.com',
  OutputSecurityType : "11",
  OutGoingEmail : 'email@gmail.com',
  Layouts: []
}
