import { Injectable } from '@angular/core';
import { HTTPDataService } from '../common/httpservice';
import { map } from "rxjs/operators";
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class CompanyLayoutsService {
    data: any = {};
    constructor(private httpservice: HTTPDataService, private http: HttpClient) {

    }

    getItems(tablename: string, page: number, pagesize: number, sortby: string, sortorder: boolean, searchText: string, showAll:number): Observable<any> {
        var Url = '/layout/getall/' + tablename + '/' + page + '/' + pagesize + '/' + sortby + '/' + sortorder + '/'+showAll +'/'+ searchText;
        console.log(Url);
        return this.httpservice.get(Url)
        .pipe(map((res: any) => res.json()));
    }
    getItem(tablename: string, fieldNames: string, publicID: string): Observable<any> {
        var Url = '/layout/getitem/' + tablename + '/' + fieldNames + '/' + publicID;
        return this.httpservice.get(Url)
        .pipe(map((res: any) => res.json()));
    }
    ItemCodeExists(itemcode: string): Observable<any> {
        this.data={'ItemCode':'5','CompanyName':'CompanyName - 05'};
        return of(this.data);
    }

    Save(tablename: string, ViewModel: any, id: string): Observable<any> {
        // var Url = 'stock/saveitem';
        var Url = '/savelayout/' + tablename;
        console.log(ViewModel);
        return this.httpservice.post(Url, JSON.stringify(ViewModel))
        .pipe(map((res: any) => res.json()));

    }

    Delete(tablename: string, ViewModel: any, id: string): Observable<any> {
        var Url = '/layout/deleteitem/' + 'tablename' +'/'+ id;
        return this.httpservice.post(Url, JSON.stringify(ViewModel))
        .pipe(map((res: any) => res.json()));

    }

    // GetName(): Observable<any> {
    //   var Url = '/getname';
    //   return this.httpservice.get(Url)
    //   .pipe(map((res: any) => res.json()));
    // }

}
