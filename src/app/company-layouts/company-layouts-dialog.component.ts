import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'company-layouts-dialog',
  templateUrl: 'company-layouts-dialog.component.html',
})
export class CompanyLayoutsDialog {

  constructor(public dialogRef: MatDialogRef<CompanyLayoutsDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {}

}
