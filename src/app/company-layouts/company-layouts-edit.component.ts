import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ElementRef, ViewChild, ViewChildren, Inject, AfterViewInit } from '@angular/core';
import { CompanyLayoutsService } from './company-layouts.services';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogComponent } from '../common/dialog/dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormArray, ValidatorFn, FormGroupDirective, FormControl, ValidationErrors, AbstractControl} from '@angular/forms';
import { Location } from '@angular/common';
import { ValidationService, ControlMessagesComponent } from '../common/validationservice';
import { Observable, throwError, Subscription } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { MatPaginator, MatSort, MatTable, MatTableDataSource, Sort } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { SessionStorageService } from '../common/sessionstorage';
import { map, startWith } from 'rxjs/operators';
import * as _ from 'lodash';
import { resolve } from 'q';
import { CanvasComponent } from './canvas.component'
import { UploadFileComponent } from '../common/uploader/upload-file.component';
import { HttpClient } from '@angular/common/http';
import { ConstCustomerModel, ConstFieldFormModel, ConstColumnFormModel, ConstDocumentModel, ConstPageArray, ConstColumn } from './company-layouts.model';
import { CompanyLayoutsDialog } from './company-layouts-dialog.component';
import { CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

export interface DataSource {
    name: string;
}

@Component({
    selector: 'company-layouts-edit',
    templateUrl: './company-layouts-edit.component.html',
    styleUrls: ['./company-layouts.component.css']
})
export class CompanyLayoutsEditComponent implements OnInit, OnDestroy, AfterViewInit {

    acceptedMimeTypes = [
        'application/pdf',
        'image/png'
    ];

    @ViewChild(MatTable) table: MatTable<any>;
    @ViewChild('canvasComponent') canvasComp: CanvasComponent;
    @ViewChild('fileInput') fileInput: ElementRef;
    @ViewChild('uploadFileComponent') uploadFileComp: UploadFileComponent;
    pdfimage = "/assets/images/document_800_2.png";
    // pdfimage = '';
    public currentEditItem: any;
    public hasError: boolean = false;
    public nodata: boolean = false;
    public editForm: FormGroup;
    public loading: boolean = false;
    public inEditMode: boolean = false;
    public form: FormGroup;
    public isLoadingResults: boolean = true;
    public modeEdit: number = 1;
    public fieldForm: FormGroup;
    public columnForm: FormGroup;
    public firstFormGroup: FormGroup;
    public currentEditField: any;
    public backupEditField: any;
    public currentEditColumn: any;
    public backupEditColumn: any;
    public currentEditDocument: any;
    private subscriptions = new Subscription();
    pageArray: any = [];

    clientID = 0;
    fileDataUri: any = "";
    errorMsg: any = "";
    uploaderFiles: any = [];
    headerData: any = [];

    documentIsOpened = false;
    itemExists: boolean = false;
    canEdit = false;
    isDisabled = false;
    isLocked = false;
    tabDisabled = false;
    duplicateCaption = false;
    selectedIndex: number = 0;
    fileType = {
      type: '',
      classTwo: '',
      classThree: ''
    };
    selectedPage: number = 1;
    documentIndex: number = 0;
    inputDocumentTypes = [];
    transportMethods = [];
    securityProtocols = [];
    outputDocumentTypes = [];
    lookUpData = [];
    types = [];
    columnTypes = [];
    columns = ConstColumn;
    tablePositions = ["Table Fixed", "Floating Top", "Floating Bottom", "Floating Top & Bottom"];
    //public controls;
    constructor(private route: ActivatedRoute, private router: Router,
        private CompanyLayoutsService: CompanyLayoutsService, private toastr: ToastrService,
        private fb: FormBuilder, private SessionStorageService: SessionStorageService,
        private location: Location, public dialog: MatDialog, private http: HttpClient) {
        this.canEdit = SessionStorageService.getAccess(2);
        // load up Look Up Tables
        var that = this;
        console.log("In Edit");

    }
    public ngAfterViewInit(): void {

    }
    previewFile() {
        const file = this.fileInput.nativeElement.files[0];
        if (file && this.validateFile(file)) {

            const reader = new FileReader();
            reader.readAsDataURL(this.fileInput.nativeElement.files[0]);
            reader.onload = () => {
                this.fileDataUri = reader.result;
            }
        } else {
            this.errorMsg = 'File must be jpg, png, or gif and cannot be exceed 500 KB in size'
        }
    }

    setbackgroundImage() {
        var that = this;
         this.http.get("https://upliv97iwg.execute-api.ap-southeast-2.amazonaws.com/dev/data-distill-dataextractor/getImage/"+this.clientID+"/1")
            .subscribe(
                res => {
                    console.log(res);
                    this.loadPageFields(this.selectedPage);
                    if (res != null) {
                      var result = res as any;
                      console.log("Image:", result.image);
                      that.canvasComp.source = "data:image/png;base64," + result.image;
                      that.canvasComp.setBackgroundImage();
                      this.selectedIndex = 1;
                    } else {
                      this.selectedIndex = 2;
                      this.toastr.error('Possible not upload yet', 'Image not found');
                    };
                },
                err => {
                    console.log(err);
                    this.errorMsg = 'Could not upload image.';
                }
            );
    }
    validateFile(file) {
        return this.acceptedMimeTypes.includes(file.type) && file.size < 5000000;
    }
    fileUploaderChanged(files: any) {
      this.uploaderFiles = files;
      console.log("fileUploaderChanged", this.uploaderFiles);
      if (this.fileType.type == 'csv') {
        let input = this.uploaderFiles[0].data;
        let rows = input.split('\n');
        let header = rows.shift();
        this.headerData = header.split(',');
        if (this.headerData.length == -1) return;
        this.documentIndex = this.currentEditItem.Documents.map(x => {return x.ID}).indexOf(this.currentEditDocument.ID);
        this.initializeLayout();
        for (let header of this.headerData) {
          this.currentEditField = Object.assign({}, ConstFieldFormModel);
          this.currentEditField.ID = this.revisedRandId();
          this.currentEditField.Document_ID = this.currentEditDocument.ID;
          this.currentEditField.FieldName = header;
          this.currentEditItem.Documents[this.documentIndex].Layouts[0].fieldArray.push(this.currentEditField);
        }
        this.headerData = this.currentEditItem.Documents[this.documentIndex].Layouts[0].fieldArray;

        console.log("header", this.headerData, this.currentEditItem);
      }
    }
    validateUploader() {
        if (this.fileType.type == 'pdf') {
          let source = null;
          source = this.canvasComp.source;
          if (source != null) {
              const dialogRef = this.dialog.open(CompanyLayoutsDialog, {
                width: '550px',
                data: { dialog: 'Layout'}
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.uploadFileToAWS();
                }
              });
          } else {
            this.uploadFileToAWS();
          }
        };
        this.tabDisabled = false;
        this.selectedIndex = 1;
    }
    uploadFileToAWS() {
        if (this.uploaderFiles.length > 0) {
          for (let file of this.uploaderFiles) {
            let that = this;
            const base64File = file.data.split(',')[1];
            const data = { 'image': base64File, 'clientID': this.clientID };
            console.log("data:", data);
            this.http.post("https://upliv97iwg.execute-api.ap-southeast-2.amazonaws.com/dev/data-distill-dataextractor/fileupload", data)
                      .subscribe(
                          res => {
                              // handle success
                              // reset file input
                              console.log(res);
                              that.setDeletedLayouts();
                              that.ClearAllSelections();
                              that.setbackgroundImage();
                              this.isDisabled = false;
                              // this.fileInput.nativeElement.value = '';
                          },
                          err => {
                              this.errorMsg = 'Could not upload image.';
                          }
                      );
            }
        }
    }
    getAllSelections() {
        var divs = this.canvasComp.getAllSelections();
        console.log("divs:", divs);
    }
    addRectangles() {
        var rects = [];
        rects.push({ x: 98.9063, y: 160, w: 293, h: 20, id: 'Frederick_1' });
        rects.push({ x: 98.9063, y: 260, w: 293, h: 20, id: 'Frederick_2' });
        rects.push({ x: 98.9063, y: 360, w: 293, h: 20, id: 'Frederick_3' });
        rects.push({ x: 98.9063, y: 460, w: 293, h: 20, id: 'Frederick_4' });
        rects.push({ x: 98.9063, y: 560, w: 293, h: 20, id: 'Frederick_5' });

        this.canvasComp.addRectangles(rects);
    }
    getDataFromSelections() {
    }
    ClearAllSelections() {
        this.currentEditField = null;
        this.currentEditColumn = null;
        this.canvasComp.clearAllSelections();
    }
    AddRectangle() {
        var rect: any = {};
        rect.x = 371;
        rect.y = 29;
        rect.w = 520;
        rect.h = 75;
        rect.id = "Frederick_";
        this.canvasComp.addRectangle(rect);
    }
    redirectTo(uri) {
        console.log(uri);
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
            this.router.navigate([uri]));
    }

    ngOnInit() {
        this.clientID = this.route.snapshot.params['id'];
        console.log("In Edit");
        this.inEditMode = true;
        var id = this.route.snapshot.params['id'];
        console.log(id === undefined);
        if (id !== null) {
            if (id == 0)
                this.inEditMode = false;
            else
                this.inEditMode = true;
            this.load(id);
        }
        console.log("edit mode", this.inEditMode)
        this.setCompanyForm();
        this.setFieldForm();
        this.setColumnForm();
        this.setFirstForm();
    };
    setCompanyForm() {
        this.form = this.fb.group({
            CompanyName: ['', Validators.compose([Validators.required])],
            Active: ['']
        });
        this.subscriptions.add(this.form.valueChanges.subscribe((changes) => {
              for (let propName in changes) {
                  this.currentEditItem[propName] = changes[propName];
              }
          })
        )
    }
    setFieldForm() {
        this.fieldForm = this.fb.group({
            ID: [''],
            Caption: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.onKeyChange.bind(this)])],
            FieldName: [''],
            DataType: [''],
            DateValue: ['', Validators.compose([Validators.required, Validators.pattern('[DMY\/-]{8,}')])],
            MinValue: ['', Validators.compose([Validators.required, Validators.min(0)])],
            MaxValue: ['', Validators.compose([Validators.required, Validators.max(999999)])],
            DefaultValue: [''],
            EmptyValue: [''],
            Output_DataType: ['', Validators.required],
            Output_DateValue: ['', Validators.compose([Validators.required, Validators.pattern('[DMY\/-]{8,}')])],
            Output_MinValue: ['', Validators.compose([Validators.required, Validators.min(0)])],
            Output_MaxValue: ['', Validators.compose([Validators.required, Validators.max(999999)])],
            Output_DefaultValue: [''],
            Output_EmptyValue: [''],
        });
        this.subscriptions.add(
          this.fieldForm.valueChanges.subscribe((changes) => {
            for (let propName in changes) {
              this.currentEditField[propName] = changes[propName];
            }
          })
        )
    }
    setColumnForm() {
        this.columnForm = this.fb.group({
          ID: [''],
          Caption: ['', Validators.compose([Validators.required, Validators.maxLength(50), this.onKeyChangeTabular.bind(this)])],
          DataType: [''],
          DateValue: ['', Validators.compose([Validators.required, Validators.pattern('[DMY\/-]{8,}')])],
          MinValue: ['', Validators.compose([Validators.required, Validators.min(0)])],
          MaxValue: ['', Validators.compose([Validators.required, Validators.max(999999)])],
          DefaultValue: [''],
          EmptyValue: [''],
          Output_DataType: ['', Validators.required],
          Output_DateValue: ['', Validators.compose([Validators.required, Validators.pattern('[DMY\/-]{8,}')])],
          Output_MinValue: ['', Validators.compose([Validators.required, Validators.min(0)])],
          Output_MaxValue: ['', Validators.compose([Validators.required, Validators.max(999999)])],
          Output_DefaultValue: [''],
          Output_EmptyValue: [''],
          TablePosition: ['', Validators.required],
          TableTopValue: ['', Validators.required],
          TableBottomValue: ['', Validators.required]
        })
        this.subscriptions.add(this.columnForm.valueChanges.subscribe((changes) => {
            for (let propName in changes) {
              this.currentEditColumn[propName] = changes[propName];
            }
          })
        )
    }
    setFirstForm() {
        this.firstFormGroup = this.fb.group({
          DocumentName: ['', Validators.required],
          DocumentDescription: ['', Validators.required],
          InputDocumentType: ['', Validators.required],
          InputMethod: ['', Validators.required],
          OutputDocumentType: ['', Validators.required],
          OutputMethod: ['', Validators.required],
          InputSecurityType: ['', Validators.required],
          IncomingEmail: ['', Validators.required],
          OutputSecurityType: ['', Validators.required],
          OutGoingEmail: ['', Validators.required]
        });
        this.subscriptions.add(this.firstFormGroup.valueChanges.subscribe((changes) => {
            for (let propName in changes) {
              this.currentEditDocument[propName] = changes[propName];
            }
          })
        )
    }
    openDocument(doc, index) {
        console.log(doc, index);
        this.currentEditDocument = doc;
        this.documentIndex = this.currentEditItem.Documents.map(x => {return x.ID}).indexOf(this.currentEditDocument.ID);
        console.log(this.currentEditItem.Documents[this.documentIndex].Layouts);
        this.handleSelectedFileType(doc);
        this.editDocument(doc);
        this.documentIsOpened = true;
        this.setbackgroundImage();
    }
    setmode() {
        if (this.modeEdit == 1)
            this.modeEdit = 2;
        else
            this.modeEdit = 1;
    }
    ngOnDestroy() {
      this.subscriptions.unsubscribe();
    }

    async getItem(publicID: string) {
        return this.CompanyLayoutsService.getItem('Company', '*', publicID).
            subscribe(data => {
                if (data !== null)
                    this.itemExists = true;
                else
                    this.itemExists = false;
            },
                err => {
                    this.itemExists = false;
                },
                () => {
                    return this.itemExists;
                });
    }

    load(id: string) {
      console.log("id:", id)
        this.isLoadingResults = true;
        this.CompanyLayoutsService.getItem('Company', '*', id)
            .subscribe(
                data => {
                    console.log("data", data);
                    if (data) {
                        this.initializeForms();
                        this.currentEditItem = data.Data;
                        this.lookUpData = data.LookUpData;
                        this.setDocumentDropDownParameter(data.LookUpData);
                        this.form.setValue({CompanyName: this.currentEditItem.CompanyName, Active: this.currentEditItem.Active});
                        console.log("After:", this.currentEditItem);
                        this.isLoadingResults = false;
                    }
                },
                err => {
                    // this.toastr.error('Error Creating Tag.', 'Error Creating Tag.');
                    this.isLoadingResults = false;
                    console.log(err);
                });
    }
    save() {
        if (!this.form.valid) return;
        console.log(this.currentEditItem);
        console.log("barcode 1", this.form.get('tbl_refItemUIM_Barcode___BarCo'));
        console.log("barcode 2", this.form.get('tbl_refItemUIM_Barcode___BarCode'));

        this.isLoadingResults = true;
        var  saveData = {
          Data: null
        };

        saveData.Data = this.currentEditItem;
        this.CompanyLayoutsService.Save('Company', saveData, this.route.snapshot.params['id'])
            .subscribe(
                data => {
                    console.log(data);

                    if (data !== null) {
                        //this.toastr.success('Zone  Created','Zone Created');
                        this.isLoadingResults = false;
                        if (!this.inEditMode) {
                            this.inEditMode = true;
                            // this.redirectTo('/home/company-layouts/' + data.Data.publicID + '/edit');
                            this.router.navigate(['/home/company-layouts', data.Data.publicID,'edit']);
                        }
                        else {
                            // reload data
                            this.load(this.route.snapshot.params['id']);
                            //this.router.navigate(['/home/stock']);
                            //this.location.back();
                        }
                        this.toastr.success("Data Saved!!!", 'Data Saved');
                    }
                    else {
                        if (data !== null && data.ErrorDescription.length > 0) {
                            for (let entry of data.ErrorDescription) {
                                //this.toastr.error('Possible Duplicate Zone Number', 'Error Creating Zone.');
                            }
                        }
                        else {

                        }
                    }
                    this.isLoadingResults = false;
                },
                err => {
                    this.toastr.error("Data Not Saved!!!", 'Save Error');
                    this.isLoadingResults = false;
                    console.log(err);
                });
    }
    cancel() {
        //this.location.back();
        this.redirectTo('/home/company-layouts');
    }
    private _filter(value: any, TableType: number) {
        const filterValue = value.toLowerCase();
        console.log("filterValue", filterValue);
        console.log("dataType", TableType);
        return "";


    }

    documentSave() {
        console.log("saving this", this.currentEditItem);
        this.documentIsOpened = false;
    }
    documentCancel() {
        this.documentIsOpened = false;
        this.initializeForms();
    }

    onSelect(rectangle: any) {
        console.log("rectangle Selected:", rectangle);
        console.log("ID:", rectangle.rectangle.id);
        console.log("Y:", rectangle.rectangle.y);
        console.log("X:", rectangle.rectangle.x);
        console.log("B:", rectangle.rectangle.b);
        console.log("R:", rectangle.rectangle.r);
        this.setCurrentEditField(rectangle);
    }

    afterCreateRectangle(rectangle: any) {
        console.log("rectangle:", rectangle);
        console.log("ID:", rectangle.rectangle.id);
        console.log("Y:", rectangle.rectangle.y);
        console.log("X:", rectangle.rectangle.x);
        console.log("B:", rectangle.rectangle.b);
        console.log("R:", rectangle.rectangle.r);
        this.setCurrentEditField(rectangle);
    }
    afterResize(rectangle: any) {
        console.log("rectangle Resize:", rectangle);
        console.log("ID:", rectangle.rectangle.id);
        console.log("Y:", rectangle.rectangle.y);
        console.log("X:", rectangle.rectangle.x);
        console.log("B:", rectangle.rectangle.b);
        console.log("R:", rectangle.rectangle.r);
        this.setCurrentEditField(rectangle);
    }
    afterMove(rectangle: any) {
        console.log("rectangle Move:", rectangle);
        console.log("ID:", rectangle.rectangle.id);
        console.log("Y:", rectangle.rectangle.y);
        console.log("X:", rectangle.rectangle.x);
        console.log("B:", rectangle.rectangle.b);
        console.log("R:", rectangle.rectangle.r);
        this.setCurrentEditField(rectangle);
    }
    deleteRectangle(rectangle: any) {
        console.log("rectangle:", rectangle);
        this.currentEditField = this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray.find(x => {return x.ID == rectangle.rectangle.id});
        this.currentEditField.ShowLayout = false;
        console.log("currentEditField", this.currentEditField, "array", this.currentEditItem.Documents[this.documentIndex].Layouts);
    }
    canvasReady(canvas: any) {
      if (canvas) {
        this.loadPageFields(this.selectedPage);
        this.selectedIndex = 1;
      }
    }
    setCurrentEditField(rectangle: any) {
        this.initializeForms();
        this.currentEditField = this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray.find(x => {return x.ID == rectangle.rectangle.id});
        if (!this.currentEditField) {
          this.currentEditField = Object.assign({}, ConstFieldFormModel);
          this.currentEditField.Document_ID = this.currentEditDocument.ID;
          this.currentEditField.PageNo = this.selectedPage;
          this.currentEditField.tabularFieldArray = [];
        };
        console.log("value found", this.currentEditField);
        if (this.currentEditField.Output_DataType == 21) {
          this.handleSelectColumn('1');
        }
        this.currentEditField.ID = rectangle.rectangle.id;
        this.currentEditField.y = rectangle.rectangle.y;
        this.currentEditField.x = rectangle.rectangle.x;
        this.currentEditField.b = rectangle.rectangle.b;
        this.currentEditField.r = rectangle.rectangle.r;
        this.currentEditField.top_int = rectangle.rectangle.top;
        this.currentEditField.left_int = rectangle.rectangle.left;
        this.currentEditField.width = rectangle.rectangle.width;
        this.currentEditField.height = rectangle.rectangle.height;
        this.setFieldFormValue();
    }
    setFieldFormValue() {
        this.selectedIndex = 1;
        this.backupEditField = Object.assign({}, this.currentEditField);
        this.fieldForm.setValue({
            ID: this.currentEditField.ID,
            Caption: this.currentEditField.Caption,
            FieldName: this.currentEditField.FieldName,
            DataType: this.currentEditField.DataType,
            DateValue: this.currentEditField.DateValue,
            MinValue: this.currentEditField.MinValue,
            MaxValue: this.currentEditField.MaxValue,
            DefaultValue:this.currentEditField.DefaultValue,
            EmptyValue:this.currentEditField.EmptyValue,
            Output_DataType: this.currentEditField.Output_DataType,
            Output_DateValue: this.currentEditField.Output_DateValue,
            Output_MinValue: this.currentEditField.Output_MinValue,
            Output_MaxValue: this.currentEditField.Output_MaxValue,
            Output_DefaultValue:this.currentEditField.Output_DefaultValue,
            Output_EmptyValue:this.currentEditField.Output_EmptyValue,
        });
    }
    loadPageFields(page) {
        this.ClearAllSelections();
        if (!this.currentEditItem.Documents[this.documentIndex].Layouts[page-1].fieldArray) return
        this.loadRectangles(this.currentEditItem.Documents[this.documentIndex].Layouts[page-1].fieldArray);
    }
    loadRectangles(fieldArray) {
        this.initializeForms();
        var rects = [];
        if (fieldArray.length != 0) {
          for (let field of fieldArray) {
            if (field.ShowLayout) {
              rects.push({x: field.left_int, y: field.top_int , w: field.width , h: field.height , id: field.ID});
            }
          }
          this.canvasComp.addRectangles(rects);
        }
    }
    handleSelectColumn(value) {
        let index = this.currentEditField.tabularFieldArray.map(x => {return x.ColumnID}).indexOf(Number(value));
        if (index == -1) {
          this.currentEditColumn = Object.assign({}, ConstColumnFormModel);
          this.currentEditColumn.ColumnID = Number(value);
          this.currentEditColumn.PageNo = this.selectedPage;
          this.currentEditColumn.ID = this.revisedRandId();
          if ( this.currentEditField.publicID ) {
            this.currentEditColumn.Parent_ID = this.currentEditField.ID;
            this.currentEditColumn.Document_ID = this.currentEditField.Document_ID;
          }
        } else {
          let column = this.currentEditField.tabularFieldArray[index];
          this.currentEditColumn = JSON.parse(JSON.stringify(column));
        }
        this.setColumnFieldValue();
    }
    handleSelectDataType(dataType) {
        if (dataType != 21) return;
        this.handleSelectColumn('1')
    }
    setColumnFieldValue() {
        this.backupEditColumn = Object.assign({}, this.currentEditColumn);
        this.columnForm.setValue({
          ID: this.currentEditColumn.ID,
          Caption: this.currentEditColumn.Caption,
          DataType: this.currentEditColumn.DataType,
          DateValue: this.currentEditColumn.DateValue,
          MinValue: this.currentEditColumn.MinValue,
          MaxValue: this.currentEditColumn.MaxValue,
          DefaultValue: this.currentEditColumn.DefaultValue,
          EmptyValue: this.currentEditColumn.EmptyValue,
          Output_DataType: this.currentEditColumn.Output_DataType,
          Output_DateValue: this.currentEditColumn.Output_DateValue,
          Output_MinValue: this.currentEditColumn.Output_MinValue,
          Output_MaxValue: this.currentEditColumn.Output_MaxValue,
          Output_DefaultValue: this.currentEditColumn.Output_DefaultValue,
          Output_EmptyValue: this.currentEditColumn.Output_EmptyValue,
          TablePosition: this.currentEditColumn.TablePosition,
          TableTopValue: this.currentEditColumn.TableTopValue,
          TableBottomValue: this.currentEditColumn.TableBottomValue
        })
    }
    deleteChange() {
        let index = this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray.map(x => {return x.ID}).indexOf(this.currentEditField.ID);
        if (index != -1) {
          this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray[index].ShowLayout = false;
        }
        if (this.fileType.type != 'csv') this.isLocked = false;
        this.loadPageFields(this.selectedPage);
    }
    saveChange() {
        if (this.currentEditColumn != null && this.currentEditField.Output_DataType == '21') {
          console.log("saving column");
          let tabularIndex = this.currentEditField.tabularFieldArray.map(x => {return x.ColumnID}).indexOf(this.currentEditColumn.ColumnID);
          if (tabularIndex != -1) {
            this.currentEditField.tabularFieldArray[tabularIndex] = JSON.parse(JSON.stringify(this.currentEditColumn));
          } else {
            this.currentEditField.tabularFieldArray.push(JSON.parse(JSON.stringify(this.currentEditColumn)));
          };
        };
        let index = this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray.map(x => {return x.ID}).indexOf(this.currentEditField.ID);
        if (index == -1) {
          this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray.push(Object.assign({}, this.currentEditField));
        } else {
          this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray[index] = Object.assign({}, this.currentEditField);
        };
        this.backupEditField = Object.assign({}, this.currentEditField);
        this.backupEditColumn = Object.assign({}, this.currentEditColumn);
        console.log("page array", this.currentEditItem.Documents[this.documentIndex].Layouts);
        if (this.fileType.type != 'csv') this.isLocked = false;
        this.fieldForm.markAsPristine();
        this.fieldForm.markAsUntouched();
        this.columnForm.markAsPristine();
        this.columnForm.markAsUntouched();
    }
    revertChange() {
        this.currentEditField = Object.assign({}, this.backupEditField);
        this.currentEditColumn = Object.assign({}, this.backupEditColumn);
        this.setFieldFormValue();
        if (this.currentEditColumn.Caption != null) this.setColumnFieldValue();
    }
    addDocument() {
        this.currentEditDocument = Object.assign({}, ConstDocumentModel);
        this.currentEditDocument.Company_ID = this.currentEditItem.ID;
        this.handleSelectedFileType(null);
        console.log("add new Document", this.currentEditDocument, this.currentEditItem);
        this.editDocument(this.currentEditDocument);
    }
    editDocument(doc: any) {
        this.selectedIndex = 0;
        this.documentIsOpened = true;
        this.firstFormGroup.setValue({
            DocumentName: doc.DocumentName,
            DocumentDescription: doc.DocumentDescription,
            InputDocumentType: doc.InputDocumentType,
            InputMethod: doc.InputMethod,
            OutputDocumentType: doc.OutputDocumentType,
            OutputMethod: doc.OutputMethod,
            InputSecurityType: doc.InputSecurityType,
            IncomingEmail: doc.IncomingEmail,
            OutputSecurityType: doc.OutputSecurityType,
            OutGoingEmail: doc.OutGoingEmail
        });
    }
    confirmDocument() {
        if (this.currentEditDocument.ID != null) {
          console.log("update documents");
          let index = this.currentEditItem.Documents.map(x => {return x.ID}).indexOf(this.currentEditDocument.ID);
          this.currentEditItem.Documents[index] = this.currentEditDocument;
        } else {
          console.log("add documents");
          this.currentEditDocument.ID = this.revisedRandId();
          this.currentEditItem.Documents.push(this.currentEditDocument);
          this.documentIndex = this.currentEditItem.Documents.length-1;
          this.selectedIndex = 2;
          this.tabDisabled = true;
          this.initializeLayout();
        }
        console.log("documents", this.currentEditItem.Documents);
        if (this.currentEditDocument.InputDocumentType == "3") {
          this.fileType.type = "pdf";
          this.isDisabled = false;
        };
        if (this.currentEditDocument.InputDocumentType == "2") {
          this.fileType.type = "csv";
          this.isDisabled = true;
        };
        this.handleSelectedFileType(this.currentEditDocument);
    }
    revisedRandId() {
        return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
    }
    deleteDocument(doc: any) {
        let index = this.currentEditItem.Documents.map(x => {return x.ID}).indexOf(doc.ID);
        this.currentEditItem.Documents[index].ShowDocument = false;
    }
    handleSelectHeader(event) {
        if (event.target.value == '') return;
        let index = this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray.map(x => {return x.FieldName}).indexOf(event.target.value);
        this.currentEditField = this.currentEditItem.Documents[this.documentIndex].Layouts[this.selectedPage-1].fieldArray[index];
        console.log("currentEditField",  this.currentEditField);
        this.setFieldFormValue();
    }
    initializeLayout() {
        this.currentEditItem.Documents[this.documentIndex].Layouts = JSON.parse(JSON.stringify(ConstPageArray));
        console.log("currentEditItem", this.currentEditItem);
        this.initializeForms()
    }
    initializeForms() {
        this.fieldForm.markAsPristine();
        this.fieldForm.markAsUntouched();
        this.columnForm.markAsPristine();
        this.columnForm.markAsUntouched();
        this.currentEditField = null;
        this.currentEditColumn = null;
    }
    handleSelectedFileType(doc: any) {
        if (doc == null) {
          this.fileType.type = '';
          this.fileType.classTwo = 'box two two-new';
          this.fileType.classThree = 'box three three-new';
        } else if (doc.InputDocumentType == '3') {
          this.fileType.type = 'pdf';
          this.fileType.classTwo = 'box two two-pdf';
          this.fileType.classThree = 'box three three-pdf';
          this.isLocked = false;
        } else {
          this.fileType.type = 'csv';
          this.fileType.classTwo = 'box two two-csv';
          this.fileType.classThree = 'box three three-csv';
          this.headerData = [];
          this.isLocked = true;
          this.currentEditField = Object.assign({}, ConstFieldFormModel);
        }
        console.log("fileType", this.fileType);
    }
    setDocumentDropDownParameter(parameter) {
      console.log(parameter);
      if ( this.inputDocumentTypes.length != 0 ) return;
      parameter.forEach((param) => {
        if( param.TableType == 1 ) {
          this.inputDocumentTypes.push(param);
          if ( param.Description != 'MYOB' && param.Description != 'XERO' )
          this.outputDocumentTypes.push(param);
        };
        if ( param.TableType == 2 ) {
          this.transportMethods.push(param);
        };
        if ( param.TableType == 3 ) {
          this.securityProtocols.push(param);
        };
        if ( param.TableType == 4 ) {
          this.types.push(param);
          if ( param.Description != 'Tabular Data') this.columnTypes.push(param);
        };
      });
    }

    onKeyChange(control: FormControl): {[s: string]: boolean} | null {
      if (this.currentEditField == null) return null;
      let form = <FormGroup>control.root;
      for (let layout of this.currentEditItem.Documents[this.documentIndex].Layouts) {
        for (let field of layout.fieldArray) {
          if ( field.ShowLayout && (form.controls['ID'].value != field.ID) && (field.Caption == control.value)) {
            return { duplicate: true }
          }
          for (let tabular of field.tabularFieldArray) {
            if ( tabular.ShowLayout && (form.controls['ID'].value != tabular.ID) && (tabular.Caption == control.value)) {
              return { duplicate: true }
            }
          }
        }
      }
      return null;
    }
    onKeyChangeTabular(control: FormControl): {[s: string]: boolean} | null {
      if (this.currentEditColumn == null) return null;
      if (this.currentEditField.Caption == control.value) return { duplicate: true };
      return this.onKeyChange(control);
    }

    drop(event: CdkDragDrop<string[]>) {
      console.log(event);
      moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
    }
    setDeletedLayouts() {
      this.currentEditItem.Documents[this.documentIndex].Layouts.forEach(page => {
        page.fieldArray.forEach(field => {
          field.ShowLayout = false;
          field.tabularFieldArray.forEach(tabular => {
            tabular.ShowLayout = false;
          });
        });
      });
    }

}
