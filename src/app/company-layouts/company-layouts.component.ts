import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ViewContainerRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { MatPaginator, MatSort, MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { CompanyLayoutsService } from './company-layouts.services';
import { DialogComponent } from '../common/dialog/dialog.component';
import { CompanyLayoutsDialog } from './company-layouts-dialog.component'
import { SessionStorageService } from '../common/sessionstorage';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'company-layouts',
  templateUrl: './company-layouts.component.html',
  styleUrls: ['./company-layouts.component.css']
})
export class CompanyLayoutsComponent implements OnInit {
  ShowInactive=1;
  canAdd=false;
  modalTitle: string;
  public items: any[];
  private sub: any;
  private navigate: any;
  displayedColumns = ['actions','CompanyName'];
  resultsLength = 0;
  public filter: string = "";
  isLoadingResults = false;
  public currentEditItem: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private route: ActivatedRoute, private router: Router, private CompanyLayoutsService: CompanyLayoutsService,
    private SessionStorageService: SessionStorageService, private toastr: ToastrService,
    vcr: ViewContainerRef, public dialog: MatDialog) {
      this.canAdd = SessionStorageService.getAccess(1);
  }

  ngOnInit() {
    this.navigate = this.router.events.subscribe((event:any) => {
      // when reload current page
      if (event instanceof NavigationEnd) {
        this.sub.unsubscribe();
        this.fetchData();
      }
    })
    this.fetchData();
  };
  ngOnDestroy() {
    this.sub.unsubscribe();
    this.navigate.unsubscribe();
  }
  fetchData() {
    console.log("fetch Data");
    this.sub = this.route.params.subscribe(params => {
      this.paginator.pageIndex = 0;
      this.sort.active = "CompanyName";
      this.filter = '';
      this.sort.direction="asc";
      console.log("OnInit - this.paginator.pageIndex", this.paginator.pageIndex);

      var tempPageIndex = this.SessionStorageService.getItem("companylayout_pageindex");
      if(tempPageIndex !== null && tempPageIndex !== "" && tempPageIndex !== undefined)
      {
        this.paginator.pageIndex = tempPageIndex;
      }
      var tempstock_activesort = this.SessionStorageService.getItem("companylayout_activesort");
      if(tempstock_activesort !== null && tempstock_activesort !== "" && tempstock_activesort !== undefined)
      {
        this.sort.active = tempstock_activesort;
      }
      var tempstock_filter = this.SessionStorageService.getItem("companylayout_filter");
      if(tempstock_filter !== null && tempstock_filter !== "" && tempstock_filter !== undefined)
      {
        this.filter = tempstock_filter;
      }
      var tempstock_sortdirection= this.SessionStorageService.getItem("companylayout_sortdirection");
      if(tempstock_sortdirection !== null && tempstock_sortdirection !== "" && tempstock_sortdirection !== undefined)
      {
        this.sort.direction = tempstock_sortdirection;
      }
      var tempstock_showactive = this.SessionStorageService.getItem("companylayout_showactive");
      if(tempstock_showactive !== null && tempstock_showactive !== "" && tempstock_showactive !== undefined)
      {
        this.ShowInactive = tempstock_showactive;
      }

      //this.loadCustomers(this.paginator.pageIndex, this.sort.active, this.sort.direction == "asc");
      //this.paginator.pageIndex, 10, sortby, sortorder, ''

      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          startWith({}),
          switchMap(() => {
            this.isLoadingResults = true;

            this.SessionStorageService.setItem("companylayout_pageindex",this.paginator.pageIndex);
            this.SessionStorageService.setItem("companylayout_activesort",this.sort.active);
            this.SessionStorageService.setItem("companylayout_filter",this.filter);
            this.SessionStorageService.setItem("companylayout_sortdirection",this.sort.direction );
            this.SessionStorageService.setItem("companylayout_showactive",this.ShowInactive);

            // debugger;
            console.log("this.paginator.pageIndex", this.paginator.pageIndex);
            console.log("this.sort.active:", this.sort.active == "undefined");
            console.log("this.sort.direction:", this.sort.direction);
            return this.CompanyLayoutsService.getItems(
              'Company',
              this.paginator.pageIndex + 1, 10,
              this.sort.active == undefined ? "CompanyName" : this.sort.active, this.sort.direction == "" ? false : this.sort.direction !== "asc",
              this.filter,
              this.ShowInactive);
          }),
          map(data => {

            // Flip flag to show that loading has finished.
            this.isLoadingResults = false;
            this.resultsLength = data.RecordCount;
            console.log("Data:",data.Data);
            return data.Data;
          }),
          catchError(() => {
            this.isLoadingResults = false;
            // Catch if the GitHub API has reached its rate limit. Return empty data.
            return observableOf([]);
          })
        ).subscribe(data => this.items = data);

    });
  }
  applyFilter(filterValue: string) {
    //filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.filter = filterValue;
    console.log(this.filter);
    this.paginator.pageIndex = 0;
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  onChangeActive(isActive: number) {
    //filterValue = filterValue.trim(); // Remove whitespace
    this.ShowInactive = isActive;
    console.log(this.filter);
    this.paginator.pageIndex = 0;
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  addnew() {
    console.log("addnew clicked")
    this.router.navigate(['/home/company-layouts', 0,'edit']);
  }

  getItem(publicID: string) {
    return this.CompanyLayoutsService.getItem("Company", "*", publicID);
  }

  delete(docData: any) {
    const dialogRef = this.dialog.open(CompanyLayoutsDialog, {
      width: '550px',
      data: { dialog: 'Record'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.CompanyLayoutsService.Delete('Company', docData, docData.publicID).subscribe(
          data => {
            if (data != null) {
              this.router.navigate(['/home/company-layouts']);
              this.toastr.success("Record Deleted!!!", 'Record Deleted');
            };
          },
          err => {
              this.toastr.error("Data Not Saved!!!", 'Save Error');
              console.log(err);
          }
        );
      };
    });
  }

  clone(publicID: string) {
    console.log("clone ID", publicID);
    this.CompanyLayoutsService.getItem("Company", "*", publicID).subscribe(
      data => {
        if (data != null) {
          this.currentEditItem = data.Data;
          this.currentEditItem.Active = true;
          this.currentEditItem.CompanyName = "Copy of " +this.currentEditItem.CompanyName;
          this.currentEditItem.ID = null;
          this.currentEditItem.publicID = null;

          for (let doc of this.currentEditItem.Documents) {
            doc.ID = null;
            doc.Company_ID = null;
            doc.publicID = null;
            for (let page of doc.Layouts) {
              for (let layout of page.fieldArray) {
                layout.ID = null;
                layout.publicID = null;
                layout.Document_ID = null;

                for (let tabular of layout.tabularFieldArray) {
                  tabular.ID = null;
                  tabular.publicID = null;
                  tabular.Document_ID = null;
                  tabular.Parent_ID = null;
                };


              };
            };
          };

          console.log("after clone:", this.currentEditItem);
          this.save();
        };
      },
      err => {
          console.log(err);
      }
    );
  }


  save() {
      this.isLoadingResults = true;
      var  saveData = {
        Data: null
      };

      saveData.Data = this.currentEditItem;
      this.CompanyLayoutsService.Save('Company', saveData, this.currentEditItem.publicID)
          .subscribe(
              data => {
                  console.log(data);

                  if (data !== null) {
                      console.log("navigation!")
                      // this.redirectTo('/home/company-layouts')
                      this.router.navigate(['/home/company-layouts',data.Data.publicID,'edit']);

                  }
                  else {
                      if (data !== null && data.ErrorDescription.length > 0) {
                          for (let entry of data.ErrorDescription) {
                              //this.toastr.error('Possible Duplicate Zone Number', 'Error Creating Zone.');
                          }
                      }
                      else {

                      }
                  }
                  this.isLoadingResults = false;
              },
              err => {

                  this.isLoadingResults = false;
                  console.log(err);
              });

  }

  redirectTo(uri) {
      console.log(uri);
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate([uri]));
  }




}
