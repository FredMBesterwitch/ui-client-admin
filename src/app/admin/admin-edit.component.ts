import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, throwError, Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from './admin.services';
import { FormBuilder, FormGroup, Validators, FormArray, ValidatorFn, FormGroupDirective } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminEditComponent implements OnInit {

  private subscriptions = new Subscription();
  public form: FormGroup;
  public currentEditItem: any;
  public isLoadingResults: boolean = true;

  constructor(private route: ActivatedRoute, private router: Router, private adminService: AdminService, private fb: FormBuilder, private toastr: ToastrService, private location: Location) { }

  ngOnInit() {
    var id = this.route.snapshot.params['id'];
    this.load(id);
    this.setForm();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  load(id: string) {
    console.log("id:", id)
      this.isLoadingResults = true;
      this.adminService.getItem('LookUpTables', '*', id)
          .subscribe(
              data => {
                  console.log("data", data)
                  if (data) {
                      this.currentEditItem = data.Data;
                      this.form.setValue({
                        Description: this.currentEditItem.Description,
                        Active: this.currentEditItem.Active
                      })
                      console.log("After:", this.currentEditItem);
                      this.isLoadingResults = false;
                  }
              },
              err => {
                  // this.toastr.error('Error Creating Tag.', 'Error Creating Tag.');
                  this.isLoadingResults = false;
                  console.log(err);
              });
  }

  setForm() {
      this.form = this.fb.group({
          Description: ['', Validators.compose([Validators.required])],
          Active: ['']
      });
      this.subscriptions.add(this.form.valueChanges.subscribe((changes) => {
            for (let propName in changes) {
                this.currentEditItem[propName] = changes[propName];
            }
        })
      )
  }

  save() {
      if (!this.form.valid) return;
      console.log(this.currentEditItem);
      this.isLoadingResults = true;
      var  saveData = {
        Data: null
      };
      saveData.Data = this.currentEditItem;
      this.adminService.Save('LookUpTables', saveData, this.route.snapshot.params['id'])
          .subscribe(
              data => {
                  console.log(data);

                  if (data !== null) {
                      //this.toastr.success('Zone  Created','Zone Created');
                      this.isLoadingResults = false;
                      this.toastr.success("Data Saved!!!", 'Data Saved');
                  }
                  else {
                      if (data !== null && data.ErrorDescription.length > 0) {
                          for (let entry of data.ErrorDescription) {
                              //this.toastr.error('Possible Duplicate Zone Number', 'Error Creating Zone.');
                          }
                      }
                      else {

                      }
                  }
                  this.isLoadingResults = false;
              },
              err => {
                  this.toastr.error("Data Not Saved!!!", 'Save Error');
                  this.isLoadingResults = false;
                  console.log(err);
              });




  }
  cancel() {
      this.location.back();
      // this.redirectTo('/home/company-layouts');
  }

  redirectTo(uri) {
      console.log(uri);
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate([uri]));
  }

}
