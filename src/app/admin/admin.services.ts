import { Injectable } from '@angular/core';
import { HTTPDataService } from '../common/httpservice';
import { map } from "rxjs/operators";
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class AdminService {

  constructor(private httpservice: HTTPDataService, private http: HttpClient) {

  }

  getItems(tablename: string, page: number, pagesize: number, sortby: string, sortorder: boolean, searchText: string, showAll:number, lookuptableType: string): Observable<any> {
    var Url = '/lookuptables/getall/' + tablename + '/' + page + '/' + pagesize + '/' + sortby + '/' + sortorder + '/'+showAll + '/' + lookuptableType +'/'+ searchText;
    console.log(Url);
    return this.httpservice.get(Url)
    .pipe(map((res: any) => res.json()));
  }

  getItem(tablename: string, fieldNames: string, publicID: string): Observable<any> {
      var Url = '/lookuptables/getitem/' + tablename + '/' + fieldNames + '/' + publicID;
      return this.httpservice.get(Url)
      .pipe(map((res: any) => res.json()));
  }

  Save(tablename: string, ViewModel: any, id: string): Observable<any> {
      // var Url = 'stock/saveitem';
      var Url = '/savelookuptables/' + tablename;
      return this.httpservice.post(Url, JSON.stringify(ViewModel))
      .pipe(map((res: any) => res.json()));

  }



}
