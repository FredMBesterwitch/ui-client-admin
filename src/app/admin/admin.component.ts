import { Component, OnInit, OnDestroy, ViewContainerRef, ViewChild } from '@angular/core';
import { Observable, throwError, Subscription, merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from './admin.services';
import { MatPaginator, MatSort } from '@angular/material';
import { SessionStorageService } from '../common/sessionstorage';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  lookuptableType: string  = '';
  displayedColumns: string[] = ['actions', 'Description'];
  dataSource: any = [];
  private sub: any;
  resultsLength = 0;
  public filter: string = "";
  isLoadingResults = false;
  ShowInactive=1;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private route: ActivatedRoute, private router: Router, private adminService: AdminService, private SessionStorageService: SessionStorageService) {}

  ngOnInit() {
    this.subscriptions.add(this.route.params.subscribe((params) => {
        this.lookuptableType = params['lookuptableType'];
        this.paginator.pageIndex = 0;
        this.sort.active = "Description";
        this.filter = '';
        this.sort.direction="asc";
        console.log("OnInit - this.paginator.pageIndex", this.paginator.pageIndex, this.sort.active, this.filter, this.sort.direction, this.ShowInactive);

        var tempPageIndex = this.SessionStorageService.getItem("lookuptable_pageindex"+this.lookuptableType);
        if(tempPageIndex !== null && tempPageIndex !== "" && tempPageIndex !== undefined)
          this.paginator.pageIndex = tempPageIndex;
        var tempstock_activesort = this.SessionStorageService.getItem("lookuptable_activesort"+this.lookuptableType);
        if(tempstock_activesort !== null && tempstock_activesort !== "" && tempstock_activesort !== undefined)
          this.sort.active = tempstock_activesort;
        var tempstock_filter = this.SessionStorageService.getItem("lookuptable_filter"+this.lookuptableType);
        if(tempstock_filter !== null && tempstock_filter !== "" && tempstock_filter !== undefined)
          this.filter = tempstock_filter;
        var tempstock_sortdirection= this.SessionStorageService.getItem("lookuptable_sortdirection"+this.lookuptableType);
        if(tempstock_sortdirection !== null && tempstock_sortdirection !== "" && tempstock_sortdirection !== undefined)
          this.sort.direction = tempstock_sortdirection;
        var tempstock_showactive = this.SessionStorageService.getItem("lookuptable_showactive"+this.lookuptableType);
        if(tempstock_showactive !== null && tempstock_showactive !== "" && tempstock_showactive !== undefined)
          this.ShowInactive = tempstock_showactive;





        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page)
          .pipe(
            startWith({}),
            switchMap(() => {
              this.isLoadingResults = true;

              this.SessionStorageService.setItem("lookuptable_pageindex"+this.lookuptableType, this.paginator.pageIndex);
              this.SessionStorageService.setItem("lookuptable_activesort"+this.lookuptableType, this.sort.active);
              this.SessionStorageService.setItem("lookuptable_filter"+this.lookuptableType, this.filter);
              this.SessionStorageService.setItem("lookuptable_sortdirection"+this.lookuptableType, this.sort.direction );
              this.SessionStorageService.setItem("lookuptable_showactive"+this.lookuptableType, this.ShowInactive);

              // debugger;
              console.log("this.paginator.pageIndex", this.paginator.pageIndex);
              console.log("this.sort.active:", this.sort.active == "undefined");
              console.log("this.sort.direction:", this.sort.direction);
              
              return this.adminService.getItems(
                'LookUpTables',
                this.paginator.pageIndex + 1, 10,
                this.sort.active == undefined ? "Description" : this.sort.active, this.sort.direction == "" ? false : this.sort.direction !== "asc",
                this.filter,
                this.ShowInactive,
                this.lookuptableType);
            }),
            map(data => {

              // Flip flag to show that loading has finished.
              this.isLoadingResults = false;
              this.resultsLength = data.RecordCount;
              console.log("Data:",data.Data);
              return data.Data;
            }),
            catchError(() => {
              this.isLoadingResults = false;
              // Catch if the GitHub API has reached its rate limit. Return empty data.
              return observableOf([]);
            })
          ).subscribe(data => this.dataSource = data);
      })
    );
  }

  ngOnDestroy() {
      this.subscriptions.unsubscribe();
  }

  applyFilter(filterValue: string) {
    //filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.filter = filterValue;
    console.log(this.filter);
    this.paginator.pageIndex = 0;
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  onChangeActive(isActive: number) {
    //filterValue = filterValue.trim(); // Remove whitespace
    this.ShowInactive = isActive;
    console.log(this.filter);
    this.paginator.pageIndex = 0;
    this.paginator._changePageSize(this.paginator.pageSize);
  }

}
