var appConfig = {
    // "rootWebApiUrl":"https://wa9o8pplej.execute-api.ap-southeast-2.amazonaws.com/dev/jljdataextractor/",
    "rootWebApiUrl":"http://localhost:8000",
    "authorizeURL":"http://localhost:5050/authenticate/token",
    "revokeURL":"http://localhost:5050/authenticate/logout",
    "ClientID":"",
    "AppName":"",
    "AppLogo":"assets/images/companylogo.png",
    "CompanyName":"Beacon Lighting"
}
